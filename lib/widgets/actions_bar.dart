import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/widgets/buttons/icon_button.widget.dart';
import 'package:ptk_mobile/widgets/buttons/popup_button.widget.dart';

Widget popupUseCase(BuildContext context) => Scaffold(
      backgroundColor: themeColors(context).primary,
      body: ActionsBar(
        backIcon: DriingIcons.settings,
        onTapBack: () {},
        actions: [
          PopupButton(
            onSelected: (value) {},
            entries: [
              const PopupMenuItem(
                height: AppResources.spacerXXl,
                value: 0,
                child: PopupItem(
                  text: 'Obtenir le lien',
                  icon: Icons.link_rounded,
                ),
              ),
              const PopupMenuItem(
                height: AppResources.spacerXXl,
                value: 1,
                child: PopupItem(
                  text: 'Paramètres',
                  icon: Icons.settings_rounded,
                ),
              ),
              PopupMenuItem(
                height: AppResources.spacerXXl,
                value: 2,
                child: PopupItem(
                  text: "Quitter l'évènement",
                  icon: Icons.exit_to_app_rounded,
                  color: themeColors(context).red,
                ),
              ),
            ],
          ),
        ],
      ),
    );

class ActionsBar extends StatefulWidget {
  final bool backArrow;
  final List<Widget>? actions;
  final void Function()? onTapBack;
  final IconData? backIcon;

  const ActionsBar({
    this.onTapBack,
    this.backArrow = true,
    this.backIcon,
    this.actions,
  });

  @override
  _ActionsBarState createState() => _ActionsBarState();
}

class _ActionsBarState extends State<ActionsBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppResources.spacerM)
          .add(const EdgeInsets.only(top: AppResources.spacerS)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          if (widget.backArrow)
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                if (widget.backIcon != null)
                  Icon(widget.backIcon, color: Colors.black12, size: 35),
                DIconButton(
                  icon: Icons.chevron_left_rounded,
                  onTap: widget.onTapBack ?? () => context.pop(),
                )
              ],
            ),
          if (widget.actions != null)
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: widget.actions!,
              ),
            ),
        ],
      ),
    );
  }
}
