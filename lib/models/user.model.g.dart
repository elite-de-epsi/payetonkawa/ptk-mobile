// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as int,
      uid: json['uid'] as String?,
      username: json['username'] as String?,
      language: json['language'] as String?,
      birthday: json['birthday'] == null
          ? null
          : DateTime.parse(json['birthday'] as String),
      title: json['title'] as String?,
      level: json['level'] as int?,
      xp: json['xp'] as int?,
      isContributor: json['isContributor'] as bool? ?? false,
      xpBoost: (json['xpBoost'] as num?)?.toDouble(),
      phoneNumber: json['phoneNumber'] as String?,
      profileImage: json['profileImage'] as String?,
      email: json['email'] as String?,
      creation: json['creation'] as String?,
      roles:
          (json['roles'] as List<dynamic>?)?.map((e) => e as String).toList(),
      modification: json['modification'] as String?,
      socialConnectionsUsed: (json['socialConnectionsUsed'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'username': instance.username,
      'language': instance.language,
      'birthday': instance.birthday?.toIso8601String(),
      'title': instance.title,
      'level': instance.level,
      'xp': instance.xp,
      'xpBoost': instance.xpBoost,
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'profileImage': instance.profileImage,
      'creation': instance.creation,
      'modification': instance.modification,
      'roles': instance.roles,
      'isContributor': instance.isContributor,
      'socialConnectionsUsed': instance.socialConnectionsUsed,
    };
