import 'package:json_annotation/json_annotation.dart';

part 'user.model.g.dart';

@JsonSerializable()
class User {
  int id;
  String? uid;
  String? username;
  String? language;
  DateTime? birthday;
  String? title;
  int? level;
  int? xp;
  double? xpBoost;
  String? phoneNumber;
  String? email;
  String? profileImage;
  String? creation;
  String? modification;
  List<String>? roles;
  bool? isContributor;
  List<String>? socialConnectionsUsed;

  User({
    required this.id,
    this.uid,
    this.username,
    this.language,
    this.birthday,
    this.title,
    this.level,
    this.xp,
    this.isContributor = false,
    this.xpBoost,
    this.phoneNumber,
    this.profileImage,
    this.email,
    this.creation,
    this.roles,
    this.modification,
    this.socialConnectionsUsed,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
