import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:ptk_mobile/views/auth/email/email.view.dart';
import 'package:ptk_mobile/views/auth/qr_code/qr_code.view.dart';
import 'package:ptk_mobile/views/products/ar/ar.view.dart';
import 'package:ptk_mobile/views/products/product/product.view.dart';
import 'package:ptk_mobile/views/products/products.view.dart';

final _shellNavigatorKey = GlobalKey<NavigatorState>();

List<RouteBase> routes = [
  DGoRoute(
    path: ProductsView.route,
    builder: (context, state) => const ProductsView(),
    routes: <RouteBase>[
      DGoRoute(
        path: ':id',
        builder: (context, state) =>
            ProductView(int.parse(state.params['id'] ?? '0')),
      ),
    ],
  ),
  DGoRoute(
    path: EmailView.route,
    builder: (context, state) => const EmailView(),
  ),
  DGoRoute(
    path: QrCodeView.route,
    builder: (context, state) => const QrCodeView(),
  ),
  DGoRoute(
    path: '/ar-view',
    builder: (context, state) => const LocalAndWebObjectsView(),
  ),
  // ShellRoute(
  //   navigatorKey: _shellNavigatorKey,
  //   pageBuilder: (BuildContext context, GoRouterState state, Widget child) =>
  //       MaterialPage(
  //     child: HeroControllerScope(
  //       controller: MaterialApp.createMaterialHeroController(),
  //       child: BottomNavigation(state, child),
  //     ),
  //   ),
  //   routes: <RouteBase>[],
  // ),
];

class DGoRoute extends GoRoute {
  DGoRoute({
    required String path,
    Widget Function(BuildContext, GoRouterState)? builder,
    GoRouterRedirect? redirect,
    List<RouteBase> routes = const <RouteBase>[],
  }) : super(
          redirect: redirect,
          path: path,
          routes: routes,
          builder: builder,
          pageBuilder: (context, state) => buildPageWithDefaultTransition<void>(
            context: context,
            state: state,
            child: builder?.call(context, state) ?? Container(),
          ),
        );
}

CustomTransitionPage buildPageWithDefaultTransition<T>({
  required BuildContext context,
  required GoRouterState state,
  required Widget child,
}) {
  return CustomTransitionPage<T>(
    key: state.pageKey,
    child: child,
    transitionsBuilder: (context, animation, secondaryAnimation, child) =>
        FadeTransition(opacity: animation, child: child),
  );
}
