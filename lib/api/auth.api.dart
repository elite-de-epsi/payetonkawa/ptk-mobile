import 'dart:convert';

import 'package:ptk_mobile/api/config.api.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:ptk_mobile/utils/classes/token.class.dart';

class AuthAPI {
  static Future<Response<bool>> sendEmail(
    String email,
  ) async {
    return ConfigAPI.requestErrorHandler<bool>(
      request: ConfigAPI.getHttp(
        route: '/auth/send-email',
        type: ResponseType.post,
        body: {
          "email": email,
        },
      ),
      cases: {
        201: (response) async {
          return Response.complete<bool>(true);
        },
      },
    );
  }

  static Future<Response<Token>> confirmEmail(
    String code,
    String email,
  ) async {
    return ConfigAPI.requestErrorHandler<Token>(
      request: ConfigAPI.getHttp(
        route: '/auth/check-code',
        type: ResponseType.post,
        body: {
          "code": code,
          "email": email,
        },
      ),
      cases: {
        201: (response) async {
          final jsonResponse = jsonDecode(response.body);
          final Token token = Token.fromJson(jsonResponse);
          return Response.complete<Token>(token);
        },
      },
    );
  }
}
