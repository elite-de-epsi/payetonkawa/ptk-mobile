class ImageFolders {
  ImageFolders._();

  static const $CustomerImageFolder customer = $CustomerImageFolder();
  static const $CocktailImageFolder cocktail = $CocktailImageFolder();
}

class $CustomerImageFolder {
  const $CustomerImageFolder();
  String get bucket => 'driing-customer';
  String get profilePicture => 'profile-picture';
}

class $CocktailImageFolder {
  const $CocktailImageFolder();
  String get bucket => 'driing-cocktail';
  String get presentations => 'presentations';
}
