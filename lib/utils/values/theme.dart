import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';

const circe = 'Circe';

ThemeData appLightTheme() => ThemeData(
      extensions: const <CustomColors>[
        CustomColors.light,
      ],
      textTheme: textTheme,
      inputDecorationTheme: inputDecorationTheme,
      bottomSheetTheme: bottomSheetTheme,
      dialogTheme: dialogTheme,
    );

ThemeData appDarkTheme() => ThemeData(
      extensions: const <CustomColors>[
        CustomColors.dark,
      ],
      textTheme: textTheme,
      inputDecorationTheme: inputDecorationTheme,
      bottomSheetTheme: bottomSheetTheme,
      dialogTheme: dialogTheme,
    );

const bottomSheetTheme = BottomSheetThemeData(
  shape: AppResources.shapeRoundedTop,
);

const dialogTheme = DialogTheme(
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(15)),
  ),
);

const inputDecorationTheme = InputDecorationTheme(
  contentPadding: EdgeInsets.symmetric(horizontal: AppResources.spacerM),
  focusedErrorBorder: InputBorder.none,
  border: InputBorder.none,
  focusedBorder: InputBorder.none,
  enabledBorder: InputBorder.none,
  errorBorder: InputBorder.none,
  disabledBorder: InputBorder.none,
  isDense: true,
  fillColor: DColors.red,
);

const textTheme = TextTheme(
  displayLarge: TextStyle(
    fontFamily: circe,
    fontSize: 24,
    fontWeight: FontWeight.w900,
    color: DColors.white,
  ),
  displayMedium: TextStyle(
    fontFamily: circe,
    fontSize: 20,
    fontWeight: FontWeight.w900,
    color: DColors.white,
  ),
  displaySmall: TextStyle(
    fontFamily: circe,
    fontSize: 18,
    fontWeight: FontWeight.w700,
    color: DColors.white,
  ),
  headlineMedium: TextStyle(
    fontFamily: circe,
    fontSize: 16,
    fontWeight: FontWeight.w700,
    color: DColors.white,
  ),
  headlineSmall: TextStyle(
    fontFamily: circe,
    fontSize: 14,
    fontWeight: FontWeight.w300,
    color: DColors.black,
  ),
  bodyLarge: TextStyle(
    fontFamily: circe,
    fontSize: 16,
    height: 1.5,
    fontWeight: FontWeight.normal,
    color: DColors.black,
  ),
  bodyMedium: TextStyle(
    fontFamily: circe,
    fontSize: 14,
    height: 1.5,
    fontWeight: FontWeight.normal,
    color: DColors.black,
  ),
  labelLarge: TextStyle(
    fontFamily: circe,
    fontSize: 14,
    height: 1.5,
    fontWeight: FontWeight.w900,
    color: DColors.black,
  ),
);

@immutable
class CustomColors extends ThemeExtension<CustomColors> {
  const CustomColors({
    required this.primary,
    required this.primaryDark,
    required this.primaryLight,
    required this.red,
    required this.green,
    required this.black,
    required this.white,
    required this.gray,
    required this.grayLight,
    required this.grayDark,
  });
  final Color primary;
  final Color primaryDark;
  final Color primaryLight;
  final Color red;
  final Color green;
  final Color black;
  final Color white;
  final Color gray;
  final Color grayLight;
  final Color grayDark;

  @override
  CustomColors copyWith({
    Color? primary,
    Color? primaryDark,
    Color? primaryLight,
    Color? red,
    Color? green,
    Color? black,
    Color? white,
    Color? gray,
    Color? grayLight,
    Color? grayDark,
  }) {
    return CustomColors(
      primary: primary ?? this.primary,
      primaryDark: primaryDark ?? this.primaryDark,
      primaryLight: primaryLight ?? this.primaryLight,
      red: red ?? this.red,
      green: green ?? this.green,
      black: black ?? this.black,
      white: white ?? this.white,
      gray: gray ?? this.gray,
      grayLight: grayLight ?? this.grayLight,
      grayDark: grayDark ?? this.grayDark,
    );
  } // Controls how the properties change on theme changes

  @override
  CustomColors lerp(ThemeExtension<CustomColors>? other, double t) {
    if (other is! CustomColors) {
      return this;
    }
    return CustomColors(
      primary: Color.lerp(primary, other.primary, t)!,
      primaryDark: Color.lerp(primaryDark, other.primaryDark, t)!,
      primaryLight: Color.lerp(primaryLight, other.primaryLight, t)!,
      red: Color.lerp(red, other.red, t)!,
      green: Color.lerp(green, other.green, t)!,
      black: Color.lerp(black, other.black, t)!,
      white: Color.lerp(white, other.white, t)!,
      gray: Color.lerp(gray, other.gray, t)!,
      grayDark: Color.lerp(grayDark, other.grayDark, t)!,
      grayLight: Color.lerp(grayLight, other.grayLight, t)!,
    );
  } // Controls how it displays when the instance is being passed

  // to the `print()` method.
  @override
  String toString() => 'CustomColors('
      'primary: $primary, primaryDark: $primaryDark, primaryLight: $primaryLight, danger: $red'
      ')'; // the light theme

  static const light = CustomColors(
    primary: DColors.primary,
    primaryDark: DColors.primaryDark,
    primaryLight: DColors.primaryLight,
    red: DColors.red,
    green: DColors.green,
    black: DColors.black,
    white: DColors.white,
    gray: DColors.gray,
    grayDark: DColors.darkGray,
    grayLight: DColors.lightGray,
  ); // the dark theme
  static const dark = CustomColors(
    primary: Color(0xff1F2128),
    primaryDark: Color(0xff000000),
    primaryLight: DColors.primaryLight,
    red: DColors.red,
    green: DColors.green,
    black: DColors.black,
    white: Color(0xff000000),
    gray: DColors.gray,
    grayDark: DColors.darkGray,
    grayLight: DColors.lightGray,
  );
}
