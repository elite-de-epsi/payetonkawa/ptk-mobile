// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_tile.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductTile _$ProductTileFromJson(Map<String, dynamic> json) => ProductTile(
      id: json['id'] as String,
      name: json['name'] as String,
      price: json['price'] as String,
      color: json['color'] as String,
    );

Map<String, dynamic> _$ProductTileToJson(ProductTile instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
      'color': instance.color,
    };
