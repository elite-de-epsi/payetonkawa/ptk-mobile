// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token.class.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Token _$TokenFromJson(Map<String, dynamic> json) => Token(
      access_token: json['access_token'] as String,
      refresh_token: json['refresh_token'] as String,
      token_type: json['token_type'] as String,
      scope: json['scope'] as String,
      expires_in: json['expires_in'] as int,
    );

Map<String, dynamic> _$TokenToJson(Token instance) => <String, dynamic>{
      'access_token': instance.access_token,
      'refresh_token': instance.refresh_token,
      'token_type': instance.token_type,
      'scope': instance.scope,
      'expires_in': instance.expires_in,
    };
