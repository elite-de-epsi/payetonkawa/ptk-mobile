import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/widgets/buttons/ink_well_effect.dart';

class DTextButton extends StatelessWidget {
  final List<TextSpan> texts;
  final Color? color;
  final void Function() onTap;

  const DTextButton({
    super.key,
    required this.texts,
    required this.onTap,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return InkWellEffect(
      backgroundColor: Colors.transparent,
      borderRadius: 10,
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: AppResources.spacerS, vertical: AppResources.spacerXs),
        child: RichText(
          text: TextSpan(
              children: texts,
              style: themeTexts(context).bodyMedium!.copyWith(color: color)),
          textAlign: TextAlign.center,
        ),
      ),
      onTap: onTap,
    );
  }
}

/*@widgetbookKnobs.WidgetbookUseCase(name: 'example', type: DTextButton)
Widget textButtonUseCase(BuildContext context) => Scaffold(
      backgroundColor: themeColors(context).primary,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppResources.spacerM),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DTextButton(texts: [
              TextSpan(
                  text: context.knobs
                      .text(label: 'Title', initialValue: 'Lorem Ipsum ?')),
              TextSpan(
                  text: context.knobs.text(
                      label: 'Bold text', initialValue: 'dolor sit amet')),
            ], onTap: () {}),
          ],
        ),
      ),
    );*/
