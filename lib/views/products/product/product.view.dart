import 'package:flutter/material.dart';
import 'package:ptk_mobile/models/product.model.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/views/products/product/product.vm.dart';
import 'package:ptk_mobile/widgets/actions_bar.dart';
import 'package:ptk_mobile/widgets/buttons/button.base.dart';
import 'package:ptk_mobile/widgets/buttons/main_button.widget.dart';
import 'package:ptk_mobile/widgets/d_response_builder.dart';
import 'package:ptk_mobile/widgets/d_view_model_builder.dart';
import 'package:ptk_mobile/widgets/header_title.header.dart';

class ProductView extends StatelessWidget {
  final int id;
  static const route = '/:id';
  const ProductView(this.id, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeColors(context).primary,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: DViewModelBuilder<ProductVM>(
          model: ProductVM(context, id),
          builder: (context, vm) => DStreamBuilder<Product>(
            loadingBuilder: (BuildContext context) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
            completeBuilder: (BuildContext context, value) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const ActionsBar(),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: AppResources.spacerL),
                    child: HeaderTitle(
                      backgroundIcon: Icons.shopping_basket_outlined,
                      title: value?.name ?? '',
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: themeColors(context).white,
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        ),
                      ),
                      padding: const EdgeInsets.all(AppResources.spacerM),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Description',
                                textAlign: TextAlign.left,
                                style: themeTexts(context)
                                    .headlineMedium
                                    ?.copyWith(color: DColors.black),
                              ),
                              Text(
                                value?.description ?? '',
                                textAlign: TextAlign.left,
                                style: themeTexts(context)
                                    .bodyMedium
                                    ?.copyWith(color: DColors.black),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: AppResources.spacerM),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Color',
                                  textAlign: TextAlign.left,
                                  style: themeTexts(context)
                                      .headlineMedium
                                      ?.copyWith(color: DColors.black),
                                ),
                                Text(
                                  value?.color ?? '',
                                  textAlign: TextAlign.left,
                                  style: themeTexts(context)
                                      .bodyMedium
                                      ?.copyWith(color: DColors.black),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Price',
                                textAlign: TextAlign.left,
                                style: themeTexts(context)
                                    .headlineMedium
                                    ?.copyWith(color: DColors.black),
                              ),
                              Text(
                                value?.price ?? '',
                                textAlign: TextAlign.left,
                                style: themeTexts(context)
                                    .bodyMedium
                                    ?.copyWith(color: DColors.black),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: AppResources.spacerM),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Stock',
                                  textAlign: TextAlign.left,
                                  style: themeTexts(context)
                                      .headlineMedium
                                      ?.copyWith(color: DColors.black),
                                ),
                                Text(
                                  value?.stock.toString() ?? '',
                                  textAlign: TextAlign.left,
                                  style: themeTexts(context)
                                      .bodyMedium
                                      ?.copyWith(color: DColors.black),
                                ),
                              ],
                            ),
                          ),
                          MainButton(
                            title: 'Voir en réalité augmentée',
                            onTap: () => vm.goToARView(),
                            style: ButtonBaseStyle.dark,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
            subject: vm.productResultRes,
          ),
        ),
      ),
    );
  }
}
