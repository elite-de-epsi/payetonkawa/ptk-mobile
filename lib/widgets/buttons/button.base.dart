import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';

enum ButtonState { loading, neutral, disabled, completed }

enum ButtonBaseStyle { light, primary, green, red, gray, dark, blue }

class ButtonBase extends StatelessWidget {
  ButtonBase({
    super.key,
    this.state = ButtonState.neutral,
    this.style = ButtonBaseStyle.light,
  });

  ButtonBase.complete({
    super.key,
    this.state = ButtonState.completed,
    this.style = ButtonBaseStyle.green,
  });

  ButtonBase.loading({
    super.key,
    this.state = ButtonState.loading,
    this.style = ButtonBaseStyle.green,
  });

  final ButtonState state;
  final ButtonBaseStyle? style;

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ButtonBaseBuilder extends StatelessWidget {
  const ButtonBaseBuilder(
      {Key? key, required this.buttonBuilder, required this.style})
      : super(key: key);

  final Widget Function(BuildContext context, Color color, Color textColor)
      buttonBuilder;
  final ButtonBaseStyle? style;

  @override
  Widget build(BuildContext context) {
    Color? color;
    Color? textColor;

    switch (style) {
      case ButtonBaseStyle.dark:
        color = themeColors(context).primaryDark;
        textColor = DColors.white;
        break;
      case ButtonBaseStyle.green:
        color = themeColors(context).green;
        textColor = DColors.white;
        break;
      case ButtonBaseStyle.light:
        color = DColors.white;
        textColor = themeColors(context).primary;
        break;
      case ButtonBaseStyle.red:
        color = themeColors(context).red;
        textColor = DColors.white;
        break;
      case ButtonBaseStyle.primary:
        color = themeColors(context).primary;
        textColor = DColors.white;
        break;
      case ButtonBaseStyle.gray:
        color = DColors.gray;
        textColor = DColors.lightGray;
        break;
      case ButtonBaseStyle.blue:
        color = DColors.primaryLight;
        textColor = DColors.white;
        break;
      default:
        color = themeColors(context).white;
        textColor = themeColors(context).primary;
        break;
    }

    return buttonBuilder(context, color, textColor);
  }
}
