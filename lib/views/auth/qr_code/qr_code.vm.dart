import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/providers/auth.provider.dart';
import 'package:ptk_mobile/utils/bases.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:ptk_mobile/views/products/products.view.dart';
import 'package:rxdart/rxdart.dart';

class QrCodeVM extends BaseViewModel {
  String code = "";

  late AuthProvider authProvider;

  QrCodeVM(BuildContext context) : super(context) {
    authProvider = Provider.of<AuthProvider>(context);
  }

  BehaviorSubject<Response<bool>> confirmEmailRes =
      BehaviorSubject.seeded(Response<bool>());

  _clear() {
    confirmEmailRes.add(Response<bool>());
    notifyListeners();
  }

  Future<void> confirmEmail(String code) async {
    FocusScope.of(context).unfocus();
    fetchResponse(
      () async => authProvider.confirmEmail(code),
      subject: confirmEmailRes,
      onSuccess: (_) {
        context.go(ProductsView.route);
        _clear();
      },
    );
  }
}
