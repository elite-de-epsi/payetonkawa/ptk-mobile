enum ResponseState { loading, complete, unAuthorized }

enum MessageType { info, error, success }

enum PhoneRegistrationState { undefined, registered, unregistered }

enum InputType {
  phone,
  password,
  number,
  bool,
  text,
  pinCode,
}
