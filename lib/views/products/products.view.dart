import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:ptk_mobile/models/product_tile.model.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/views/products/products.vm.dart';
import 'package:ptk_mobile/widgets/buttons/ink_well_effect.dart';
import 'package:ptk_mobile/widgets/d_response_builder.dart';
import 'package:ptk_mobile/widgets/d_view_model_builder.dart';
import 'package:ptk_mobile/widgets/header_title.header.dart';
import 'package:ptk_mobile/widgets/search_bar.dart';

class ProductsView extends StatelessWidget {
  static const route = '/products';
  const ProductsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeColors(context).primary,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: DViewModelBuilder<ProductsVM>(
          model: ProductsVM(context),
          builder: (context, vm) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(vertical: AppResources.spacerL),
                child: HeaderTitle(
                    backgroundIcon: Icons.shopping_basket_outlined,
                    title: 'Produits'),
              ),
              IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: AppResources.spacerS),
                        decoration: BoxDecoration(
                          color: themeColors(context).white,
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          ),
                        ),
                        child: Center(
                          child: SearchBar(
                              controller: TextEditingController(),
                              hint: 'Chercher un produit',
                              onChanged: (value) => vm.searchProduct(value)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  color: themeColors(context).white,
                  child: Column(
                    children: [
                      Expanded(
                        child: DStreamBuilder<List<ProductTile>>(
                          loadingBuilder: (BuildContext context) {
                            return Container();
                          },
                          completeBuilder: (BuildContext context, value) {
                            return ListView(
                              children: [
                                for (final ProductTile product in value!)
                                  ProductTileWidget(tile: product),
                              ],
                            );
                          },
                          subject: vm.searchResultRes,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProductTileWidget extends StatelessWidget {
  final ProductTile tile;

  const ProductTileWidget({Key? key, required this.tile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWellEffect(
      backgroundColor: themeColors(context).white,
      onTap: () => context.go('/products/${tile.id}'),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: AppResources.spacerM,
          vertical: AppResources.spacerXs,
        ),
        child: Row(children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tile.name,
                  style: themeTexts(context)
                      .bodyLarge
                      ?.copyWith(color: themeColors(context).black),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(right: AppResources.spacerXs),
                      child: Text(
                        '${tile.price}€',
                        style: themeTexts(context).bodyMedium?.copyWith(
                            color: themeColors(context).black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Text(
                      tile.color.toCapitalized(),
                      style: themeTexts(context)
                          .headlineSmall
                          ?.copyWith(color: themeColors(context).grayDark),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
