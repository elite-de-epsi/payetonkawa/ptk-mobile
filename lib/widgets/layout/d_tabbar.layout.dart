import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';

class DTabBar extends StatelessWidget {
  const DTabBar({Key? key, required this.tabs}) : super(key: key);

  final List<DTab> tabs;

  @override
  Widget build(BuildContext context) {
    return TabBar(
      indicator: BoxDecoration(),
      unselectedLabelStyle: themeTexts(context)
          .bodyMedium
          ?.apply(color: themeColors(context).white),
      labelStyle: themeTexts(context)
          .labelLarge
          ?.apply(color: themeColors(context).white),
      tabs: tabs,
    );
  }
}

class DTab extends StatelessWidget {
  const DTab({Key? key, required this.text, this.notificationBubble = false})
      : super(key: key);

  final String text;
  final bool notificationBubble;

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.topRight,
        children: [
          if (notificationBubble)
            Positioned(
              right: -4,
              child: Container(
                width: 11,
                height: 11,
                decoration: BoxDecoration(
                    color: themeColors(context).primaryLight,
                    borderRadius: BorderRadius.all(Radius.circular(100))),
              ),
            ),
          Text(text.toUpperCase(),
              softWrap: false, overflow: TextOverflow.fade),
        ],
      ),
    );
  }
}
