import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/providers/system.provider.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:ptk_mobile/utils/values/message.dart';
import 'package:rxdart/rxdart.dart';

abstract class BaseProvider with FetchUtils {
  SystemProvider systemProvider;

  BaseProvider({required this.systemProvider});

  @override
  void onUnAuthorized() {
    super.onUnAuthorized();
    systemProvider.logout();
  }
}

class BaseViewModel with ChangeNotifier, FetchUtils {
  final BuildContext context;

  BaseViewModel(this.context);

  @override
  void onUnAuthorized() {
    super.onUnAuthorized();
    context.read<SystemProvider>().logout();
  }

  @override
  void displayError(String error) {
    super.displayError(error);
    showError(context, error);
  }
}

abstract class FetchUtils {
  void resetSubject<T>(BehaviorSubject<Response<T?>> subject) {
    subject.add(Response<T>());
  }

  Future<Response<T>> fetchResponse<T>(
    Future<Response<T>> Function() task, {
    BehaviorSubject<Response<T>>? subject,
    void Function(T data)? onSuccess,
    void Function(String error)? onError,
    T? defaultValue,
  }) async {
    BehaviorSubject<Response<T>> _subject =
        subject ?? BehaviorSubject.seeded(Response<T>());
    try {
      /// Si une valeur par défaut est définie, on ne met pas le sujet en loading.
      if (defaultValue != null) {
        _subject.add(Response.complete<T>(defaultValue));
      } else {
        _subject.add(Response.loading<T>());
      }

      /// On exécute la tâche et on met la response obtenue dans le sujet.
      _subject.add(await task());

      /// On exécute onSuccess si il existe en passant la data du sujet (de type <T>)
      if (_subject.value.isComplete() && onSuccess != null) {
        onSuccess.call(_subject.value.data as T);

        /// Si la response == unAuthorized alors on exécute onUnAuthorized qui fera appel à la méthode logout() du SystemProvider.
      } else if (_subject.value.isUnAuthorized()) {
        onUnAuthorized();
      }
      return _subject.value;
    } catch (e) {
      _subject.addError(e);

      /// On exécute onError si il existe en passant le message d'erreur
      if (onError != null) {
        onError.call(e.toString());
      } else {
        /// Sinon, on exécute displayError() en passant le message d'erreur. Cette méthode affiche un message d'alerte à l'écran
        /// seulement si la méthode fetchResponse() est appelée depuis un ViewModel (qui contient le context).
        displayError(e.toString());
      }
      throw e;
    }
  }

  void displayError(String error) {
    debugPrint('error white fetch: ${error}');
  }

  void onUnAuthorized() {
    debugPrint('unAuthorized while fetch');
  }
}
