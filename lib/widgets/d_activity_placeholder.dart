import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:shimmer/shimmer.dart';

class DActivityPlaceholder extends StatelessWidget {
  final Widget child;
  final bool hideContent;

  const DActivityPlaceholder({
    required this.child,
    this.hideContent = false,
  });

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Shimmer.fromColors(
        baseColor: DColors.black.withOpacity(0.2),
        highlightColor: DColors.black.withOpacity(0.1),
        period: const Duration(seconds: 2),
        child: hideContent == true
            ? Stack(
                children: [
                  child,
                  Positioned.fill(
                    child: Container(
                      color: DColors.white,
                    ),
                  ),
                ],
              )
            : child,
      ),
    );
  }
}

class DActivityPlaceholderBox extends StatelessWidget {
  final double? width;
  final double? height;
  final BoxShape? shape;
  final bool noBorderRadius;

  const DActivityPlaceholderBox({
    this.width,
    this.height,
    this.shape,
    this.noBorderRadius = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: DColors.white,
        shape: shape ?? BoxShape.rectangle,
        borderRadius: noBorderRadius
            ? null
            : shape != BoxShape.circle
                ? AppResources.borderRadiusMedium
                : null,
      ),
    );
  }
}
