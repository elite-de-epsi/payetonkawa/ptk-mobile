import 'package:json_annotation/json_annotation.dart';

part 'product_tile.model.g.dart';

@JsonSerializable()
class ProductTile {
  String id;
  String name;
  String price;
  String color;

  ProductTile({
    required this.id,
    required this.name,
    required this.price,
    required this.color,
  });

  factory ProductTile.fromJson(Map<String, dynamic> json) =>
      _$ProductTileFromJson(json);

  Map<String, dynamic> toJson() => _$ProductTileToJson(this);
}
