import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/views/auth/qr_code/qr_code.vm.dart';
import 'package:ptk_mobile/widgets/d_response_builder.dart';
import 'package:ptk_mobile/widgets/d_view_model_builder.dart';

class QrCodeView extends StatefulWidget {
  static const route = '/qr_code';

  const QrCodeView({super.key});

  @override
  State<QrCodeView> createState() => _QrCodeViewState();
}

class _QrCodeViewState extends State<QrCodeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeColors(context).primary,
      body: SafeArea(
        child: DViewModelBuilder<QrCodeVM>(
          model: QrCodeVM(context),
          builder: (context, vm) => DStreamBuilder(
            loadingBuilder: (BuildContext context) {
              return const Center(child: CircularProgressIndicator());
            },
            completeBuilder: (BuildContext context, value) {
              return Container();
            },
            initialBuilder: (BuildContext context) {
              return Stack(
                children: [
                  MobileScanner(
                    onDetect: (capture) {
                      final List<Barcode> barcodes = capture.barcodes;
                      for (final barcode in barcodes) {
                        debugPrint('Barcode found! ${barcode.rawValue}');
                        vm.confirmEmail(barcode.rawValue ?? '');
                      }
                    },
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: AppResources.spacerL,
                            horizontal: AppResources.spacerM),
                        child: Center(
                          child: Text(
                            'Scannez le code que vous avez recu par mail',
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.apply(color: DColors.white),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
            subject: vm.confirmEmailRes,
          ),
        ),
      ),
    );
  }
}
