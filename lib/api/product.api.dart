import 'dart:convert';

import 'package:ptk_mobile/api/config.api.dart';
import 'package:ptk_mobile/models/product.model.dart';
import 'package:ptk_mobile/models/product_tile.model.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';

class ProductAPI {
  static Future<Response<List<ProductTile>>> getProducts() async {
    return ConfigAPI.requestErrorHandler<List<ProductTile>>(
      request: ConfigAPI.getHttp(
        route: '/products',
        type: ResponseType.get,
      ),
      cases: {
        200: (response) async {
          List<ProductTile> myProducts = [];
          final jsonResponse = jsonDecode(response.body);
          for (int i = 0; i < jsonResponse.length; i++) {
            myProducts.add(ProductTile.fromJson(jsonResponse[i]));
          }
          return Response.complete<List<ProductTile>>(myProducts);
        },
      },
    );
  }

  static Future<Response<Product>> getProduct(int id) async {
    return ConfigAPI.requestErrorHandler<Product>(
      request: ConfigAPI.getHttp(
        route: '/products/$id',
        type: ResponseType.get,
      ),
      cases: {
        200: (response) async {
          final jsonResponse = jsonDecode(response.body);
          return Response.complete<Product>(Product.fromJson(jsonResponse));
        },
      },
    );
  }
}
