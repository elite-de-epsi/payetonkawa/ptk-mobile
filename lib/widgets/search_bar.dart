import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/widgets/buttons/expand_tap_area.widget.dart';

import 'buttons/icon_button.widget.dart';

class SearchBar extends StatelessWidget {
  final String hint;
  final bool backArrow;
  final bool autoFocus;
  final bool readOnly;
  final void Function(String)? onChanged;
  final void Function()? onTap;
  final TextEditingController controller;

  SearchBar({
    required this.hint,
    this.backArrow = false,
    this.onTap,
    this.autoFocus = false,
    this.readOnly = false,
    required this.controller,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppResources.spacerM),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: AppResources.spacerS),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50)),
          color: DColors.lightGray,
        ),
        child: Row(
          children: [
            if (backArrow)
              ExpandTapArea(
                child: Padding(
                  padding: const EdgeInsets.only(right: AppResources.spacerS),
                  child: Transform.rotate(
                    angle: 180 * math.pi / 180,
                    child: DIconButton(
                      iconColor: DColors.black.withOpacity(0.4),
                      onTap: () {},
                      icon: DriingIcons.arrow,
                    ),
                  ),
                ),
                onTap: () => Navigator.of(context).pop(),
              ),
            Padding(
              padding: const EdgeInsets.only(right: AppResources.spacerS),
              child: Icon(
                Icons.search,
                size: 20,
                color: DColors.black.withOpacity(0.4),
              ),
            ),
            Expanded(
              child: TextField(
                controller: controller,
                onTap: onTap,
                readOnly: readOnly,
                onChanged: onChanged,
                style: Theme.of(context).textTheme.bodyLarge,
                autofocus: autoFocus,
                decoration: InputDecoration(
                  hintText: hint,
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding: const EdgeInsets.symmetric(vertical: 6),
                  isDense: true,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

/*@widgetbookKnobs.WidgetbookUseCase(name: 'example', type: SearchBar)
Widget searchBarUseCase(BuildContext context) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: SearchBar(
                hint: context.knobs.text(label: 'Hint', initialValue: 'Lorem Ipsum'),
                controller: TextEditingController(),
              ),
            ),
          ],
        ),
      ],
    );*/
