import 'package:flutter/material.dart';

class DColors {
  // Driing Colors
  static const primaryLight = Color(0xffC48634);
  static const primary = Color(0xff6F3F2B);
  static const primaryDark = Color(0xff212121);

  // Colors
  static const green = Color(0xFF1BC863);
  static const red = Color(0xFFCC4A4A);

  static const white = Color(0xfffef6ff);
  static const lightGray = Color(0xffE5E5E5);
  static const gray = Color(0xffC7C7C7);
  static const darkGray = Color(0xff8A8A8A);

  static const black = Color(0xff1E2124);
  static const transparent = Color(0x00000000);
}
