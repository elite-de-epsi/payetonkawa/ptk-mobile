import 'package:json_annotation/json_annotation.dart';

part 'product.model.g.dart';

@JsonSerializable()
class Product {
  String id;
  String name;
  String price;
  String color;
  String description;
  int stock;

  Product({
    required this.id,
    required this.name,
    required this.price,
    required this.color,
    required this.description,
    required this.stock,
  });

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
