import 'package:json_annotation/json_annotation.dart';

part 'token.class.g.dart';

@JsonSerializable()
class Token {
  String access_token;
  String refresh_token;
  String token_type;
  String scope;
  int expires_in;

  Token(
      {required this.access_token,
      required this.refresh_token,
      required this.token_type,
      required this.scope,
      required this.expires_in});

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);
}
