import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';
import 'package:ptk_mobile/utils/values/resources.dart';

class PopupItem extends StatelessWidget {
  const PopupItem(
      {Key? key, required this.text, required this.icon, this.color})
      : super(key: key);

  final String text;
  final IconData icon;
  final Color? color;

  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: AppResources.spacerM),
          child: Icon(
            icon,
            color: color ?? themeColors(context).black,
            size: 18,
          ),
        ),
        Text(text.toUpperCase(),
            style: themeTexts(context)
                .labelLarge
                ?.copyWith(color: color ?? themeColors(context).black)),
      ],
    );
  }
}

class PopupButton extends StatelessWidget {
  const PopupButton(
      {Key? key,
      this.icon = DriingIcons.dots_menu,
      this.iconColor,
      required this.onSelected,
      required this.entries})
      : super(key: key);

  final IconData icon;
  final Color? iconColor;
  final PopupMenuItemSelected<int> onSelected;
  final List<PopupMenuItem<int>> entries;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      color: themeColors(context).white,
      icon: Icon(
        icon,
        color: iconColor ?? themeColors(context).white,
      ),
      padding: EdgeInsets.zero,
      onSelected: onSelected,
      iconSize: 20,
      itemBuilder: (BuildContext context) => entries,
    );
  }
}
