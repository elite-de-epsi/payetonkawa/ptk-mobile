import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/api/config.api.dart';
import 'package:ptk_mobile/providers/auth.provider.dart';
import 'package:ptk_mobile/providers/product.provider.dart';
import 'package:ptk_mobile/providers/system.provider.dart';
import 'package:ptk_mobile/utils/classes/app_config.class.dart';
import 'package:ptk_mobile/utils/values/message.dart';
import 'package:ptk_mobile/utils/values/routes.dart';
import 'package:ptk_mobile/utils/values/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Widget> initializeApp(AppConfig appConfig) async {
  WidgetsFlutterBinding.ensureInitialized();

  /// Set Api URL in adequacy with current app flavor
  ConfigAPI.setApiUrl(appConfig.flavor);

  /// Get if the app must be in dark mode
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final darkModeOn = prefs.getBool('darkMode') ?? true;

  /// Load token (get in shared preferences and modify http header in adequacy with it)
  await ConfigAPI.loadToken();

  return DriingApp(
    appConfig: appConfig,
    darkModeOn: darkModeOn,
  );
}

class DriingApp extends StatelessWidget {
  final AppConfig appConfig;
  final bool darkModeOn;
  final _navigatorKey = GlobalKey<NavigatorState>();

  void loggedOutOnUi() {
    Future.delayed(Duration.zero, () {
      Navigator.of(_navigatorKey.currentContext!, rootNavigator: true)
          .pushNamedAndRemoveUntil(
        '/welcome',
        (Route<dynamic> route) => false,
      );
    });
    showInfo(
        _navigatorKey.currentContext!, 'app_logged_out_success_flash_message');
  }

  DriingApp({
    required this.darkModeOn,
    required this.appConfig,
  });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SystemProvider>(
      create: (_) => SystemProvider(
        appLightTheme(),
        loggedOutOnUi,
      ),
      child: Consumer<SystemProvider>(
        builder: (context, SystemProvider sysProvider, __) => MultiProvider(
          providers: [
            Provider(create: (_) => AuthProvider(systemProvider: sysProvider)),
            Provider(
                create: (_) => ProductProvider(systemProvider: sysProvider)),
          ],
          builder: (context, child) {
            return Selector<SystemProvider, ThemeData>(
              selector: (context, provider) => provider.getTheme,
              builder: (context, themeData, child) {
                SystemChrome.setPreferredOrientations([
                  DeviceOrientation.portraitUp,
                  DeviceOrientation.portraitDown
                ]);
                SystemChrome.setSystemUIOverlayStyle(
                  const SystemUiOverlayStyle(
                    statusBarColor: Colors.transparent,
                    statusBarBrightness: Brightness.light,
                    statusBarIconBrightness: Brightness.light,
                    systemNavigationBarColor: Colors.black,
                    systemNavigationBarIconBrightness: Brightness.light,
                  ),
                );
                return MaterialApp.router(
                  routerConfig: GoRouter(
                    navigatorKey: _navigatorKey,
                    observers: [HeroController()],
                    routes: routes,
                    initialLocation: '/email',
                  ),
                  debugShowCheckedModeBanner: false,
                  theme: themeData,
                  builder: (context, child) {
                    child = Banner(
                      location: BannerLocation.bottomStart,
                      message: appConfig.flavor,
                      color: appConfig.flavor == 'dev'
                          ? Colors.blue.withOpacity(0.6)
                          : appConfig.flavor == 'staging'
                              ? Colors.green.withOpacity(0.6)
                              : Colors.red.withOpacity(0.6),
                      textStyle: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 12.0,
                        letterSpacing: 1.0,
                      ),
                      child: child,
                    );

                    return ScrollConfiguration(
                      behavior: MyBehavior(),
                      child: child,
                    );
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
    BuildContext context,
    Widget child,
    AxisDirection axisDirection,
  ) {
    return child;
  }
}
