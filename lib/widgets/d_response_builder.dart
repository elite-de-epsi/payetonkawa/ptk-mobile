// ignore_for_file: invariant_booleans, null_check_on_nullable_type_parameter

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/enums.dart';
import 'package:rxdart/rxdart.dart';

class DStreamBuilder<ResultType> extends StatelessWidget {
  final WidgetBuilder loadingBuilder;
  final Widget Function(BuildContext context, ResultType? value)
      completeBuilder;
  final WidgetBuilder? initialBuilder;
  final bool alertUnAuthorized;
  final bool buildInitialOnError;
  final BehaviorSubject<Response<ResultType>> subject;

  const DStreamBuilder({
    this.buildInitialOnError = false,
    required this.loadingBuilder,
    required this.completeBuilder,
    this.initialBuilder,
    required this.subject,
    this.alertUnAuthorized = false,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Response<ResultType>>(
      stream: subject.stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          if (buildInitialOnError && initialBuilder != null) {
            return initialBuilder!(context);
          } else {
            return Center(
              child: Text(
                'Error occurred',
                style: themeTexts(context)
                    .bodyMedium!
                    .copyWith(color: themeColors(context).red),
              ),
            );
          }
        }
        switch (snapshot.data?.state) {
          //If response is loading, display a custom loader (shimmer)
          case ResponseState.loading:
            return loadingBuilder(context);
          //If response is complete, so we have data, call the completeBuilder (can be redirect, showMessage, or add a widget)
          case ResponseState.complete:
            return completeBuilder(context, snapshot.data?.data);
          //If response is unauthorized, logout the user
          case ResponseState.unAuthorized:
            //handled by fetchResponse method
            break;
          //If response's state is null, return default builder
          case null:
            if (initialBuilder != null) return initialBuilder!(context);
            break;
        }
        return Container();
      },
    );
  }
}
