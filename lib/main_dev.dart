import 'package:flutter/material.dart';
import 'package:ptk_mobile/app.dart';
import 'package:ptk_mobile/utils/classes/app_config.class.dart';

void main() async {
  final AppConfig devAppConfig =
      AppConfig(appName: 'PayeTonKawa Dev', flavor: 'dev');
  initializeApp(devAppConfig).then((app) => runApp(app));
}
/*
flutter run -t lib/main_dev.dart  --flavor=dev
# Debug signing configuration + dev flavor
flutter run -t lib/main_dev.dart  --debug --flavor=dev
flutter run -t lib/main_dev.dart  --release --flavor=dev
flutter build appbundle -t lib/main_dev.dart  --flavor=dev
flutter build apk -t lib/main_dev.dart  --flavor=dev

-----FIREBASE CONF
flutterfire config --project=driing-3b5b4 --out=lib/firebase_options_dev.dart --android-app-id=com.doouble.driing_app.dev --ios-bundle-id=com.doouble.driingApp.dev
*/
