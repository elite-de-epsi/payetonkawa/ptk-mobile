import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/models/product.model.dart';
import 'package:ptk_mobile/providers/product.provider.dart';
import 'package:ptk_mobile/utils/bases.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:rxdart/rxdart.dart';

class ProductVM extends BaseViewModel {
  late ProductProvider productProvider;
  final int id;

  BehaviorSubject<Response<Product>> productResultRes =
      BehaviorSubject.seeded(Response<Product>());

  ProductVM(BuildContext context, this.id) : super(context) {
    productProvider = Provider.of<ProductProvider>(context);
    getProductResultRes();
  }

  Future<void> getProductResultRes() async {
    fetchResponse<Product>(
      () => productProvider.getProduct(id),
      subject: productResultRes,
      defaultValue: productResultRes.value.data,
    );
  }

  void goToARView() {
    context.go('/ar-view');
  }
}
