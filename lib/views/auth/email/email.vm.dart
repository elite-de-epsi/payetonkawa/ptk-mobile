import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/providers/auth.provider.dart';
import 'package:ptk_mobile/utils/bases.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:ptk_mobile/views/auth/qr_code/qr_code.view.dart';
import 'package:rxdart/rxdart.dart';

class EmailVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();

  late AuthProvider authProvider;

  EmailVM(BuildContext context) : super(context) {
    authProvider = Provider.of<AuthProvider>(context);
    if (authProvider.tempEmail != null) {
      emailController.text = authProvider.tempEmail!;
    }
  }

  BehaviorSubject<Response<bool>> sendEmailRes =
      BehaviorSubject.seeded(Response<bool>());

  _clear() {
    emailController.text = '';
    formKey.currentState!.validate();
    sendEmailRes.add(Response<bool>());
    notifyListeners();
  }

  onEmailSaved(String email) {
    sendEmail();
  }

  Future<void> sendEmail() async {
    if (formKey.currentState!.validate()) {
      FocusScope.of(context).unfocus();
      fetchResponse(
        () async => authProvider.sendEmail(emailController.text),
        subject: sendEmailRes,
        onSuccess: (_) {
          _clear();
          context.go(QrCodeView.route);
        },
        onError: (error) {
          _clear();
        },
      );
    }
  }
}
