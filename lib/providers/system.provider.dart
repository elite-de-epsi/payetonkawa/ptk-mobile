import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SystemProvider extends ChangeNotifier {
  /// Shared preferences keys
  static const _access_token_key = 'user_access_token';

  ThemeData _themeData;
  void Function() _loggedOutOnUi;

  SystemProvider(this._themeData, this._loggedOutOnUi) : super() {}

  ThemeData get getTheme => _themeData;

  Future<void> setTheme(ThemeData newThemeData) async {
    _themeData = newThemeData;
    notifyListeners();
  }

  Future<void> logout({bool showOnUI = true}) async {
    /// Remove token in shared preferences
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove(_access_token_key);

    /// Show logged out on screen if asked
    if (showOnUI) _loggedOutOnUi();
  }
}
