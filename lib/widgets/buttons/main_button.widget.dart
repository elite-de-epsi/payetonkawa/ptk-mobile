import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/widgets/buttons/button.base.dart';
import 'package:ptk_mobile/widgets/buttons/ink_well_effect.dart';

class MainButton extends ButtonBase {
  MainButton(
      {Key? key,
      this.onTap,
      required this.title,
      this.icon,
      this.width,
      ButtonBaseStyle? style})
      : super(key: key, style: style, state: ButtonState.neutral);
  MainButton.complete(
      {Key? key, this.title = '', this.width, this.icon, this.onTap})
      : super(
            key: key,
            state: ButtonState.completed,
            style: ButtonBaseStyle.green);
  MainButton.loading(
      {Key? key,
      this.title = '',
      this.width,
      this.onTap,
      this.icon,
      ButtonBaseStyle? style})
      : super(key: key, style: style, state: ButtonState.loading);

  final VoidCallback? onTap;
  final String title;
  final double? width;
  final IconData? icon;

  @override
  Widget build(BuildContext context) {
    return ButtonBaseBuilder(
        style: style,
        buttonBuilder: (context, color, textColor) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(100),
              ),
              color: color,
            ),
            width: width,
            child: InkWellEffect(
              onTap: onTap ?? () {},
              backgroundColor: color,
              borderRadius: 100,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: AppResources.spacerXs,
                    horizontal: AppResources.spacerM),
                child: Center(
                  child: state == ButtonState.loading
                      ? CircularProgressIndicator(
                          color: textColor,
                        )
                      : state == ButtonState.completed
                          ? Icon(
                              DriingIcons.check,
                              color: textColor,
                              size: 21,
                            )
                          : icon != null
                              ? Icon(
                                  icon,
                                  color: textColor,
                                  size: 21,
                                )
                              : Text(
                                  title.toUpperCase(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelLarge!
                                      .apply(
                                        color: textColor,
                                      ),
                                ),
                ),
              ),
            ),
          );
        });
  }
}

/*@widgetbookKnobs.WidgetbookUseCase(name: 'example', type: MainButton)
Widget buttonUseCase(BuildContext context) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MainButton(
                onTap: () {},
                title: context.knobs.text(label: 'Text', initialValue: 'Lorem ipsum'),
                width: context.knobs.number(label: 'Width', initialValue: 200).toDouble(),
                style: context.knobs.options(
                  label: 'Style',
                  options: [
                    Option(label: 'Primary', value: ButtonBaseStyle.primary),
                    Option(label: 'Blue', value: ButtonBaseStyle.blue),
                    Option(label: 'Light', value: ButtonBaseStyle.light),
                    Option(label: 'Dark', value: ButtonBaseStyle.dark),
                    Option(label: 'Red', value: ButtonBaseStyle.red),
                    Option(label: 'Green', value: ButtonBaseStyle.green),
                    Option(label: 'Gray', value: ButtonBaseStyle.gray),
                  ],
                )),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: AppResources.spacerM),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MainButton.loading(
                  onTap: () {},
                  title: context.knobs.text(label: 'Text', initialValue: 'Lorem ipsum'),
                  width: context.knobs.number(label: 'Width', initialValue: 200).toDouble(),
                  style: context.knobs.options(
                    label: 'Style',
                    options: [
                      Option(label: 'Primary', value: ButtonBaseStyle.primary),
                      Option(label: 'Blue', value: ButtonBaseStyle.blue),
                      Option(label: 'Light', value: ButtonBaseStyle.light),
                      Option(label: 'Dark', value: ButtonBaseStyle.dark),
                      Option(label: 'Red', value: ButtonBaseStyle.red),
                      Option(label: 'Green', value: ButtonBaseStyle.green),
                      Option(label: 'Gray', value: ButtonBaseStyle.gray),
                    ],
                  )),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MainButton.complete(
              onTap: () {},
              width: context.knobs.number(label: 'Width', initialValue: 200).toDouble(),
            ),
          ],
        ),
      ],
    );*/
