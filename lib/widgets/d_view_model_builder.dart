// ignore_for_file: invariant_booleans, null_check_on_nullable_type_parameter

import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/utils/bases.dart';

class DViewModelBuilder<T extends BaseViewModel> extends StatelessWidget {
  final T model;
  final Widget Function(BuildContext context, T model) builder;

  const DViewModelBuilder({
    required this.builder,
    required this.model,
  });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => model,
      child: Consumer<T>(
        builder: (BuildContext context, T value, Widget? child) =>
            builder(context, value),
      ),
    );
  }
}
