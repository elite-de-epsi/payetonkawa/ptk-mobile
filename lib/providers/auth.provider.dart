// ignore_for_file: parameter_assignments

import 'package:ptk_mobile/api/auth.api.dart';
import 'package:ptk_mobile/api/config.api.dart';
import 'package:ptk_mobile/providers/system.provider.dart';
import 'package:ptk_mobile/utils/bases.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:ptk_mobile/utils/classes/token.class.dart';

class AuthProvider extends BaseProvider {
  String? socialConnectionType;
  String? socialUid;
  String? refreshToken;

  /// Temp variables
  String? tempEmail;

  AuthProvider({required SystemProvider systemProvider})
      : super(systemProvider: systemProvider);

  clear() {
    tempEmail = null;
    refreshToken = null;
  }

  Future<Response<bool>> sendEmail(String email) async {
    return await fetchResponse(
      () async => await AuthAPI.sendEmail(email),
      onSuccess: (_) async => tempEmail = email,
    );
  }

  Future<Response<bool>> confirmEmail(String code) async {
    final Response<Token> tokenResponse = await fetchResponse(
      () async => await AuthAPI.confirmEmail(code, tempEmail ?? ''),
      onSuccess: (token) async {
        await ConfigAPI.setToken(token.access_token);
        clear();
      },
    );
    return Response.complete<bool>(tokenResponse.isComplete());
  }
}
