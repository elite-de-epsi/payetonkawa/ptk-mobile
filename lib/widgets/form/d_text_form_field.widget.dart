import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';

enum _DTextFormFieldIndicatorState { hidden, valid, error }

class DTextFormField extends StatefulWidget {
  final IconData? prefixIcon;
  final String? prefixText;
  final String? hintText;
  final TextEditingController controller;
  final TextInputType? keyboardType;
  final FormFieldValidator? validator;
  final bool autofocus;
  final bool obscureText;
  final bool isSmall;
  final Color textColor;
  final Color? backgroundColor;
  final AutovalidateMode autovalidateMode;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputAction? textInputAction;
  final void Function(String)? onChanged;
  final void Function(String)? onSubmitted;
  final void Function()? onTap;
  final FocusNode? focusNode;

  const DTextFormField({
    this.prefixIcon,
    this.hintText,
    required this.controller,
    this.keyboardType = TextInputType.text,
    this.validator,
    this.textColor = Colors.white,
    this.backgroundColor,
    this.autofocus = false,
    this.textInputAction = TextInputAction.next,
    this.onChanged,
    this.onSubmitted,
    this.obscureText = false,
    this.inputFormatters,
    this.prefixText,
    this.autovalidateMode = AutovalidateMode.disabled,
    this.onTap,
    this.focusNode,
    this.isSmall = false,
  });

  @override
  State<DTextFormField> createState() => _DTextFormFieldState();
}

class _DTextFormFieldState extends State<DTextFormField> {
  _DTextFormFieldIndicatorState indicatorState =
      _DTextFormFieldIndicatorState.hidden;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          height: 2 * (widget.isSmall ? 0 : AppResources.spacerXl),
          decoration: BoxDecoration(
            color: widget.backgroundColor ?? themeColors(context).primaryDark,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
        ),
        Padding(
          padding: widget.prefixIcon == null && widget.prefixText == null
              ? EdgeInsets.only(
                  left: widget.isSmall ? 0 : AppResources.spacerS,
                  top: widget.isSmall ? 0 : AppResources.spacerXs,
                )
              : EdgeInsets.only(
                  top: widget.isSmall ? 0 : AppResources.spacerXs,
                ),
          child: TextFormField(
            textInputAction: widget.textInputAction,
            inputFormatters: widget.inputFormatters,
            onFieldSubmitted: widget.onSubmitted,
            onChanged: widget.onChanged,
            autofocus: widget.autofocus,
            controller: widget.controller,
            onTap: widget.onTap,
            focusNode: widget.focusNode,
            validator: (validate) {
              final value = widget.validator?.call(validate);
              _checkIndicatorState(value);
              return value;
            },
            autovalidateMode: widget.autovalidateMode,
            obscureText: widget.obscureText,
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .apply(color: widget.textColor),
            textAlignVertical: TextAlignVertical.center,
            keyboardType: widget.keyboardType,
            decoration: InputDecoration(
              prefix: widget.prefixIcon == null && widget.prefixText != null
                  ? Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppResources.spacerXs),
                      child: Text(
                        widget.prefixText!,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .apply(color: widget.textColor),
                      ),
                    )
                  : null,
              prefixIcon: widget.prefixIcon != null
                  ? Icon(
                      widget.prefixIcon,
                      size: 25,
                      color: widget.textColor,
                    )
                  : null,
              suffixIconConstraints:
                  BoxConstraints(maxWidth: 48, maxHeight: 48),
              suffixIcon: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(top: 15, right: AppResources.spacerS),
                    child: indicatorState == _DTextFormFieldIndicatorState.valid
                        ? const Icon(
                            Icons.check_circle,
                            size: 18,
                            color: Colors.green,
                          )
                        : indicatorState == _DTextFormFieldIndicatorState.error
                            ? const Icon(
                                Icons.error,
                                size: 18,
                                color: DColors.red,
                              )
                            : Container(),
                  )
                ],
              ),
              errorStyle: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .apply(color: DColors.red),
              hintStyle: Theme.of(context)
                  .textTheme
                  .bodyLarge!
                  .apply(color: widget.textColor.withOpacity(0.6)),
              hintText: widget.hintText,
            ),
          ),
        ),
      ],
    );
  }

  void _setIndicatorState(_DTextFormFieldIndicatorState state) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        indicatorState = state;
      });
    });
  }

  void _checkIndicatorState(String? value) {
    if (value == null) {
      _setIndicatorState(_DTextFormFieldIndicatorState.valid);
    } else {
      _setIndicatorState(_DTextFormFieldIndicatorState.error);
    }
  }
}
