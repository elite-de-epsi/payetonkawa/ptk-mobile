import 'package:flutter/material.dart';

class DDisabledContent extends StatelessWidget {
  final bool disabled;
  final Widget child;

  const DDisabledContent({required this.disabled, required this.child});

  @override
  Widget build(BuildContext context) {
    if (disabled) {
      return IgnorePointer(
        child: Opacity(
          opacity: 0.5,
          child: child,
        ),
      );
    }
    return child;
  }
}
