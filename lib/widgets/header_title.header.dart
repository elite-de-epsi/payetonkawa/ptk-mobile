import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';

class HeaderTitle extends StatelessWidget {
  const HeaderTitle(
      {Key? key,
      required this.backgroundIcon,
      required this.title,
      this.subtitle})
      : super(key: key);

  final IconData backgroundIcon;
  final String title;
  final String? subtitle;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          alignment: Alignment.center,
          children: [
            Icon(
              backgroundIcon,
              color: themeColors(context).primaryDark,
              size: 70,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title.toUpperCase(),
                  style: Theme.of(context)
                      .textTheme
                      .displayLarge!
                      .copyWith(color: DColors.white, height: 1.9),
                ),
                if (subtitle != null)
                  Text(
                    subtitle!,
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: DColors.white, height: 0.5),
                  )
              ],
            )
          ],
        ),
      ],
    );
  }
}
