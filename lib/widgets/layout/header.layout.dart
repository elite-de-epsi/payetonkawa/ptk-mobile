import 'package:flutter/material.dart';
import 'package:ptk_mobile/widgets/actions_bar.dart';

class HeaderLayout extends StatelessWidget {
  final String title;
  final ActionsBar? actionsBar;
  final List<Widget>? subButtons;

  const HeaderLayout({required this.title, this.actionsBar, this.subButtons});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: 200,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 25.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    title,
                    style: Theme.of(context)
                        .textTheme
                        .displaySmall!
                        .apply(color: Colors.white),
                  ),
                  if (subButtons != null) ...subButtons!,
                ],
              ),
            ),
          ),
        ),
        if (actionsBar != null)
          Positioned(
            bottom: 0,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: actionsBar,
            ),
          ),
      ],
    );
  }
}
