import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppResources {
  static const double spacerXs = 6;
  static const double spacerS = 12;
  static const double spacerM = 18;
  static const double spacerL = 24;
  static const double spacerXl = 30;
  static const double spacerXXl = 36;

  // Border Radius
  static const borderRadiusMedium = BorderRadius.all(Radius.circular(15));
  static const borderRadiusSmall = BorderRadius.all(Radius.circular(10));
  static const borderRadiusTiny = BorderRadius.all(Radius.circular(5));
  static const bottomBorderRadiusTiny =
      BorderRadius.vertical(bottom: Radius.circular(6));

  // Shape
  static const shapeRoundedRectangleMedium = RoundedRectangleBorder(
    borderRadius: AppResources.borderRadiusMedium,
  );
  static const shapeRoundedRectangleSmall = RoundedRectangleBorder(
    borderRadius: AppResources.borderRadiusSmall,
  );
  static const shapeRoundedRectangleTiny = RoundedRectangleBorder(
    borderRadius: AppResources.borderRadiusTiny,
  );
  static const shapeRoundedTop = RoundedRectangleBorder(
    borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
  );

  // Validator
  static String? emailValidator(String email) {
    return regexEmail.hasMatch(email) ? null : 'generic_invalid_email_field';
  }

  // Duration
  static const durationAnimationMedium = Duration(milliseconds: 250);
  static const longDurationCoolDown = Duration(minutes: 5);
  static const shortDurationCoolDown = Duration(minutes: 1);
  static final durationPageTransition =
      MaterialPageRoute(builder: (_) => const SizedBox()).transitionDuration;

  // Input formatter
  static TextInputFormatter get basicInputFormatter =>
      FilteringTextInputFormatter.allow(RegExp(
          r"[A-Za-zÀ-ÖØ-öø-ÿ0-9_\-?:().,'+ ]")); // Must be a new instance for each page
  static TextInputFormatter get namesInputFormatter =>
      FilteringTextInputFormatter.allow(RegExp(
          r"[A-Za-zÀ-ÖØ-öø-ÿ\-' ]")); // Must be a new instance for each page.
  static TextInputFormatter get numbersAndLettersInputFormatter =>
      // ignore: unnecessary_raw_strings
      FilteringTextInputFormatter.allow(
          RegExp(r"[A-Za-z0-9 ]")); // Must be a new instance for each page.

  // Regex
  static final regexEmail = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  static final regexDate = RegExp(r"^(\d{2})\/(\d{2})\/((?:19|20)\d{2})$");
  static final regexSixDigits = RegExp(r"^\d{6}$");
  static final RegExp regexSecuredPassword =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');

  static final regexCodeInvite = RegExp(r"^[A-Z1-9]{6}$");

  // Urls
  static const defaultImage =
      'https://i.pinimg.com/474x/66/15/da/6615da7d0d3c74c0b538cd2974e5d4ed.jpg';

  // Others
  static const boolValues = [true, false];

  static const oneYear = Duration(days: 365);
  static const moneyKeyboardType = TextInputType.numberWithOptions(
    decimal: true,
  );
}
