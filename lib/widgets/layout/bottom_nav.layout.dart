import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';

class BottomNavigation extends StatelessWidget {
  final GoRouterState state;
  final Widget child;

  BottomNavigation(this.state, this.child, {Key? key}) : super(key: key);

  final _pages = [
    const BottomNavigationBarItem(
      icon: Icon(DriingIcons.cocktails_empty),
      label: "cocktails",
      activeIcon: Icon(DriingIcons.cocktails_full),
    ),
    const BottomNavigationBarItem(
      icon: Icon(DriingIcons.dice_empty),
      label: "games",
      activeIcon: Icon(DriingIcons.dice_full),
    ),
    const BottomNavigationBarItem(
      icon: Icon(DriingIcons.user_empty),
      label: "profile",
      activeIcon: Icon(DriingIcons.user_full),
    ),
  ];

  int _calculateSelectedIndex(BuildContext context) {
    final GoRouter route = GoRouter.of(context);
    final String location = route.location;
    if (location.startsWith('/' + _pages[0].label!)) {
      return 0;
    }
    if (location.startsWith('/' + _pages[1].label!)) {
      return 1;
    }
    if (location.startsWith('/' + _pages[2].label!)) {
      return 2;
    }
    return 0;
  }

  void onTap(BuildContext context, int value) {
    switch (value) {
      case 0:
        return context.go('/' + _pages[0].label!);
      case 1:
        return context.go('/' + _pages[1].label!);
      case 2:
        return context.go('/' + _pages[2].label!);
      default:
        return context.go('/' + _pages[0].label!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: child,
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        backgroundColor: themeColors(context).white,
        iconSize: 26,
        elevation: 0,
        selectedItemColor: themeColors(context).primary,
        unselectedItemColor: themeColors(context).grayDark,
        onTap: (value) => onTap(context, value),
        currentIndex: _calculateSelectedIndex(context),
        items: _pages,
      ),
    );
  }
}
