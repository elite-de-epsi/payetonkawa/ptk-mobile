import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ResponseType { post, get, delete, put, patch }

abstract class ConfigAPI {
  static const String _sharedPreferencesTokenKey = 'user_access_token';
  static const String _prodApiUrl =
      'https://retailerapiyjnhar2c-retailer-main-api.functions.fnc.fr-par.scw.cloud';
  static const String _devApiUrl =
      'https://retailerapiyjnhar2c-retailer-release-api.functions.fnc.fr-par.scw.cloud';
  static String apiUrl = '';

  static Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };

  static void setApiUrl(String flavor) {
    apiUrl = flavor == 'prod' ? _prodApiUrl : _devApiUrl;
  }

  static Future<void> loadToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? token = prefs.getString(_sharedPreferencesTokenKey);

    _setHeadersWithToken(token);
  }

  static Future<void> setToken(String token) async {
    print('New token: $token');
    _setHeadersWithToken(token);

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_sharedPreferencesTokenKey, token);
  }

  static void _setHeadersWithToken(String? token) {
    headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
  }

  static Uri getUrl(String apiCall) {
    return Uri.parse(apiUrl + apiCall);
  }

  static Future<http.Response> getHttp({
    required String route,
    Map<String, dynamic>? body,
    bool enablePagination = false,
    int? pageNumber = 0,
    int? pageSize = 50,
    required ResponseType type,
    Map<String, dynamic>? parameters,
  }) {
    String _route = route;
    final originalParameters = parameters;
    parameters ??= {};
    if (enablePagination) {
      parameters.addAll({"size": pageSize, "page": pageNumber});
    }
    if (originalParameters != null || enablePagination) {
      _route = "$_route?";
      parameters.forEach((key, value) {
        _route = "$_route$key=$value&";
      });
      _route = _route.substring(0, _route.length - 1);
    }
    final Uri uri = getUrl(_route);
    if (type == ResponseType.post) {
      return http.post(uri, headers: headers, body: jsonEncode(body));
    } else if (type == ResponseType.delete) {
      return http.delete(uri, headers: headers, body: jsonEncode(body));
    } else if (type == ResponseType.put) {
      return http.put(uri, headers: headers, body: jsonEncode(body));
    } else if (type == ResponseType.patch) {
      return http.patch(uri, headers: headers, body: jsonEncode(body));
    } else {
      return http.get(uri, headers: headers);
    }
  }

  static Future<Response<ResultType>> requestErrorHandler<ResultType>({
    required Future<http.Response> request,
    required Map<int,
            Future<Response<ResultType>> Function(http.Response response)>
        cases,
  }) async {
    try {
      final response = await request;
      debugPrint(
        '(${response.statusCode}) ${response.request} : ${response.body}',
      );
      for (final int code in cases.keys) {
        if (response.statusCode == code) {
          return await cases[code]!(response);
        }
      }
      switch (response.statusCode) {
        case 401:
          {
            return Response.unAuthorized<ResultType>();
          }
      }
      final Map<String, dynamic> jsonResponse =
          jsonDecode(response.body) as Map<String, dynamic>;
      throw (jsonResponse['message']);
    } on SocketException {
      throw ('No Internet connection 😑');
    } on FormatException {
      throw ('Bad response format 👎');
    } on HttpException {
      throw ("Couldn't find the post 😱");
    } catch (e) {
      debugPrint(e.toString());
      throw (e.toString());
    }
  }
}
