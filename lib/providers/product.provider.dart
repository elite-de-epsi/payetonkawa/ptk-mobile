// ignore_for_file: parameter_assignments

import 'package:ptk_mobile/api/product.api.dart';
import 'package:ptk_mobile/models/product.model.dart';
import 'package:ptk_mobile/models/product_tile.model.dart';
import 'package:ptk_mobile/providers/system.provider.dart';
import 'package:ptk_mobile/utils/bases.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';

class ProductProvider extends BaseProvider {
  ProductProvider({required SystemProvider systemProvider})
      : super(systemProvider: systemProvider);

  Future<Response<List<ProductTile>>> getProducts() => ProductAPI.getProducts();
  Future<Response<Product>> getProduct(int id) => ProductAPI.getProduct(id);
}
