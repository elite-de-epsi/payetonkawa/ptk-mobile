import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/views/auth/email/email.vm.dart';
import 'package:ptk_mobile/widgets/buttons/main_button.widget.dart';
import 'package:ptk_mobile/widgets/d_response_builder.dart';
import 'package:ptk_mobile/widgets/d_view_model_builder.dart';
import 'package:ptk_mobile/widgets/form/d_text_form_field.widget.dart';

class EmailView extends StatefulWidget {
  static const route = '/email';

  const EmailView({super.key});

  @override
  State<EmailView> createState() => _EmailViewState();
}

class _EmailViewState extends State<EmailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeColors(context).primary,
      body: SafeArea(
        child: DViewModelBuilder<EmailVM>(
          model: EmailVM(context),
          builder: (context, vm) => Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: AppResources.spacerXXl),
                child: Text(
                  'Paye Ton Kawa',
                  style: themeTexts(context)
                      .headlineLarge
                      ?.apply(fontWeightDelta: 900, color: DColors.white),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(
                  bottom: 35.0,
                ),
                width: 250,
                child: Text(
                  'Trouvez votre machine à kawa préférée',
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge
                      ?.apply(color: DColors.white),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppResources.spacerM),
                  child: Form(
                    key: vm.formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        DTextFormField(
                          autofocus: true,
                          hintText: 'Adresse Email',
                          controller: vm.emailController,
                          onSubmitted: vm.onEmailSaved,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: AppResources.spacerL),
                child: Column(
                  children: [
                    DStreamBuilder<bool>(
                      subject: vm.sendEmailRes,
                      alertUnAuthorized: true,
                      loadingBuilder: (context) =>
                          MainButton.loading(width: 250),
                      completeBuilder: (context, value) =>
                          MainButton.complete(width: 250),
                      initialBuilder: (context) => MainButton(
                          onTap: vm.sendEmail,
                          title: 'Se connecter',
                          width: 250),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
