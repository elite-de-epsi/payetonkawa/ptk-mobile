import 'package:ptk_mobile/utils/values/enums.dart';

class Response<ResultType> {
  ResponseState? state;
  ResultType? data;

  Response({this.state, this.data});

  static Response<ResultType> loading<ResultType>() {
    return Response(state: ResponseState.loading);
  }

  static Response<ResultType> complete<ResultType>(ResultType data) {
    return Response(state: ResponseState.complete, data: data);
  }

  static Response<ResultType> unAuthorized<ResultType>() {
    return Response(state: ResponseState.unAuthorized);
  }

  bool isComplete() => state == ResponseState.complete;
  bool isUnAuthorized() => state == ResponseState.unAuthorized;
}
