import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/colors.dart';

class InkWellEffect extends StatelessWidget {
  const InkWellEffect(
      {Key? key,
      required this.child,
      required this.onTap,
      this.backgroundColor,
      this.borderRadius})
      : super(key: key);

  final Color? backgroundColor;
  final double? borderRadius;
  final VoidCallback onTap;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor,
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius ?? 0),
      ),
      child: InkWell(
        borderRadius: BorderRadius.all(
          Radius.circular(borderRadius ?? 0),
        ),
        splashColor: DColors.black.withOpacity(0.1),
        highlightColor: DColors.black.withOpacity(0.1),
        onTap: onTap,
        child: child,
      ),
    );
  }
}
