// ignore_for_file: parameter_assignments

import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';
import 'package:ptk_mobile/utils/values/enums.dart';
import 'package:ptk_mobile/utils/values/resources.dart';

// Store last request
_MessageData? _messageData;

// Store last controller to be able to dismiss it
FlashController? _messageController;

Future<void> showError(BuildContext context, String errorMessage) async {
  _showMessage(context, errorMessage, type: MessageType.error);
}

Future<void> showSuccess(BuildContext context, String successMessage) async {
  _showMessage(context, successMessage, type: MessageType.success);
}

Future<void> showInfo(BuildContext context, String infoMessage) async {
  _showMessage(context, infoMessage, type: MessageType.info);
}

/// Display a message to the user, like a SnackBar.
/// If a second request is made with same content, it will be ignored.
/// If a second request is made with different content, they may be overlapped.
Future<void> _showMessage(BuildContext context, String message,
    {required MessageType type,
    int? durationInSeconds,
    VoidCallback? onTap}) async {
  // Check clones
  final messageData = _MessageData(
    message: message,
    isError: type == MessageType.error,
  );
  if (messageData == _messageData) return;
  _messageData = messageData;

  //Try to get higher level context, so the Flash message's position is relative to the phone screen (and not a child widget)
  try {
    // May throw if context in not on the tree anymore
    final scaffoldContext = Scaffold.maybeOf(context)?.context;
    if (scaffoldContext != null) context = scaffoldContext;
  } catch (e) {
    // Just ignore and don't display message.
    return;
  }

  //Dismiss previous message
  _messageController?.dismiss();

  //Display new message
  await showFlash(
    context: context,
    duration: Duration(seconds: durationInSeconds ?? 3),
    builder: (context, controller) {
      final backgroundColor = type == MessageType.error
          ? DColors.red
          : type == MessageType.success
              ? DColors.green
              : Colors.lightBlue;
      _messageController = controller;

      return Flash(
        controller: controller,
        backgroundColor: backgroundColor,
        margin: const EdgeInsets.symmetric(
            horizontal: AppResources.spacerS, vertical: AppResources.spacerS),
        alignment: Alignment.topCenter,
        brightness: Brightness.dark,
        horizontalDismissDirection: HorizontalDismissDirection.horizontal,
        borderRadius: BorderRadius.circular(10.0),
        boxShadows: kElevationToShadow[4],
        onTap: () {
          controller.dismiss();
          onTap?.call();
        },
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 350),
          child: FlashBar(
            padding: const EdgeInsets.symmetric(
                horizontal: AppResources.spacerM,
                vertical: AppResources.spacerS),
            content: Row(
              children: [
                // Icon
                Icon(
                  type == MessageType.error
                      ? Icons.warning
                      : type == MessageType.success
                          ? Icons.check
                          : DriingIcons.info,
                  color: DColors.white,
                  size: 24,
                ),

                // Message
                const SizedBox(width: AppResources.spacerS),
                Expanded(
                  child: Text(
                    message,
                    style: themeTexts(context)
                        .bodyLarge!
                        .apply(color: DColors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );

  _messageData = null;
  _messageController = null;
}

class _MessageData {
  const _MessageData({required this.message, required this.isError});

  final String message;
  final bool isError;

  @override
  bool operator ==(Object o) =>
      o is _MessageData && message == o.message && isError == o.isError;

  @override
  int get hashCode => message.hashCode ^ isError.hashCode;
}
