import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ptk_mobile/models/product_tile.model.dart';
import 'package:ptk_mobile/providers/product.provider.dart';
import 'package:ptk_mobile/utils/bases.dart';
import 'package:ptk_mobile/utils/classes/response.class.dart';
import 'package:rxdart/rxdart.dart';

class ProductsVM extends BaseViewModel {
  late ProductProvider productProvider;

  List<ProductTile> tiles = [];

  BehaviorSubject<Response<List<ProductTile>>> searchResultRes =
      BehaviorSubject.seeded(Response<List<ProductTile>>());

  ProductsVM(BuildContext context) : super(context) {
    productProvider = Provider.of<ProductProvider>(context);
    getProductsResultRes();
  }

  Future<void> searchProduct(String search) async {
    searchResultRes.add(Response.complete(tiles
        .where((element) =>
            element.name.toLowerCase().contains(search.toLowerCase()))
        .toList()));
  }

  Future<void> getProductsResultRes() async {
    fetchResponse<List<ProductTile>>(
      () => productProvider.getProducts(),
      subject: searchResultRes,
      onSuccess: (data) => tiles = data,
      defaultValue: searchResultRes.value.data,
    );
  }
}
