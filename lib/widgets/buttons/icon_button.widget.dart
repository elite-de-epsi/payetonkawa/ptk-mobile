import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/widgets/buttons/expand_tap_area.widget.dart';

class DIconButton extends StatelessWidget {
  const DIconButton(
      {Key? key,
      required this.icon,
      required this.onTap,
      this.iconColor = DColors.white})
      : super(key: key);

  final IconData icon;
  final void Function() onTap;
  final Color iconColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(100),
        ),
        color: DColors.transparent,
      ),
      width: 30,
      height: 30,
      child: Material(
        color: Colors.transparent,
        borderRadius: const BorderRadius.all(
          Radius.circular(100),
        ),
        child: InkWell(
          borderRadius: const BorderRadius.all(
            Radius.circular(100),
          ),
          splashColor: DColors.black.withOpacity(0.2),
          highlightColor: DColors.black.withOpacity(0.2),
          onTap: () {},
          child: ExpandTapArea(
            onTap: onTap,
            child: Center(
              child: Icon(
                icon,
                size: 20,
                color: iconColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
