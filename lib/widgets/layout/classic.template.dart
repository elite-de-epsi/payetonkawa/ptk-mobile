import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/colors.dart';
import 'package:ptk_mobile/utils/values/resources.dart';

class ClassicTemplate extends StatelessWidget {
  final Widget content;
  final Widget header;
  final bool disableHorizontalPadding;
  final bool disableVerticalPadding;
  final Color? cardBackgroundColor;
  final Color? headerBackgroundColor;
  final ScrollController? scrollController;
  final bool hasScrollBody;
  final Widget? headerCustomBackground;
  final VoidCallback? onRefresh;

  const ClassicTemplate({
    this.onRefresh,
    required this.content,
    required this.header,
    this.cardBackgroundColor,
    this.scrollController,
    this.headerBackgroundColor,
    this.disableHorizontalPadding = false,
    this.disableVerticalPadding = false,
    this.hasScrollBody = false,
    this.headerCustomBackground,
  });

  @override
  Widget build(BuildContext context) {
    Widget child = Container(
      color: headerBackgroundColor ?? themeColors(context).primary,
      child: Stack(
        children: [
          if (headerCustomBackground != null) headerCustomBackground!,
          CustomScrollView(
            controller: scrollController ?? ScrollController(),
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: SafeArea(
                  right: false,
                  left: false,
                  bottom: false,
                  child: header,
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: hasScrollBody,
                child: Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    color: cardBackgroundColor ?? DColors.white,
                    borderRadius: const BorderRadius.vertical(
                      top: Radius.circular(30),
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal:
                          disableHorizontalPadding ? 0 : AppResources.spacerL,
                      vertical:
                          disableVerticalPadding ? 0 : AppResources.spacerM,
                    ),
                    child: content,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );

    if (onRefresh != null) {
      child = RefreshIndicator(
        backgroundColor: themeColors(context).white,
        color: themeColors(context).primary,
        onRefresh: () async {
          //onRefresh!.call();
        },
        child: child,
      );
    }
    return child;
  }
}
