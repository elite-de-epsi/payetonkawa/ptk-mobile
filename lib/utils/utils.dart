import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/values/theme.dart';

String replaceCharAt(String oldString, int index, String newChar) {
  return oldString.substring(0, index) +
      newChar +
      oldString.substring(index + 1);
}

CustomColors themeColors(context) =>
    Theme.of(context).extension<CustomColors>()!;
TextTheme themeTexts(context) => Theme.of(context).textTheme;

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
