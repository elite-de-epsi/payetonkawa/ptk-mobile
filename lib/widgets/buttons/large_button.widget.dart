import 'package:flutter/material.dart';
import 'package:ptk_mobile/utils/utils.dart';
import 'package:ptk_mobile/utils/values/driing_icons.dart';
import 'package:ptk_mobile/utils/values/resources.dart';
import 'package:ptk_mobile/widgets/buttons/button.base.dart';
import 'package:ptk_mobile/widgets/buttons/ink_well_effect.dart';

class LargeButton extends ButtonBase {
  LargeButton(
      {Key? key,
      this.icon,
      required this.text,
      this.nextArrow = false,
      ButtonBaseStyle? style,
      required this.onTap})
      : super(key: key, style: style);

  final VoidCallback onTap;
  final IconData? icon;
  final String text;
  final bool nextArrow;

  @override
  Widget build(BuildContext context) {
    return ButtonBaseBuilder(
      style: style,
      buttonBuilder: (BuildContext context, Color color, Color textColor) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(20),
            ),
            color: color,
          ),
          child: InkWellEffect(
            onTap: onTap,
            backgroundColor: color,
            borderRadius: 20,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: AppResources.spacerL),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (icon != null)
                    Padding(
                      padding: const EdgeInsets.only(
                        top: AppResources.spacerM,
                        bottom: AppResources.spacerM,
                        right: AppResources.spacerL,
                      ),
                      child: Icon(
                        icon,
                        size: 30,
                        color: textColor,
                      ),
                    ),
                  Expanded(
                    child: Text(
                      text.toUpperCase(),
                      style: themeTexts(context)
                          .displayMedium!
                          .copyWith(color: textColor),
                    ),
                  ),
                  if (nextArrow)
                    Padding(
                      padding: const EdgeInsets.only(
                        top: AppResources.spacerM,
                        bottom: AppResources.spacerM,
                        left: AppResources.spacerL,
                      ),
                      child: Icon(
                        DriingIcons.arrow_next,
                        size: AppResources.spacerM,
                        color: textColor,
                      ),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

/*@widgetbookKnobs.WidgetbookUseCase(name: 'example', type: LargeButton)
Widget largeButtonUseCase(BuildContext context) => Scaffold(
      backgroundColor: themeColors(context).primary,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppResources.spacerM),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            LargeButton(
              onTap: () {},
              text: context.knobs.text(label: 'Text', initialValue: 'Lorem ipsum'),
              nextArrow: true,
              style: context.knobs.options(
                label: 'Style',
                options: [
                  Option(label: 'Light', value: ButtonBaseStyle.light),
                  Option(label: 'Primary', value: ButtonBaseStyle.primary),
                  Option(label: 'Blue', value: ButtonBaseStyle.blue),
                  Option(label: 'Dark', value: ButtonBaseStyle.dark),
                  Option(label: 'Red', value: ButtonBaseStyle.red),
                  Option(label: 'Green', value: ButtonBaseStyle.green),
                  Option(label: 'Gray', value: ButtonBaseStyle.gray),
                ],
              ),
              icon: DriingIcons.security_lock_full,
            ),
          ],
        ),
      ),
    );*/
