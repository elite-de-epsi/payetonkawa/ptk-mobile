import 'package:flutter/cupertino.dart';
import 'package:ptk_mobile/utils/values/colors.dart';

class ExpandTapArea extends StatelessWidget {
  final VoidCallback onTap;
  final Widget child;
  final int size;
  final double? left;
  final double? right;
  const ExpandTapArea(
      {Key? key,
      this.size = 45,
      required this.child,
      required this.onTap,
      this.left = 0,
      this.right})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      clipBehavior: Clip.none,
      children: [
        child,
        Positioned(
          left: left,
          right: right,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: onTap,
            child: Container(
              width: 45,
              height: 45,
              color: DColors.transparent,
            ),
          ),
        ),
      ],
    );
  }
}
