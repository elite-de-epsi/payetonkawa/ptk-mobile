import 'package:flutter/material.dart';
import 'package:ptk_mobile/app.dart';
import 'package:ptk_mobile/utils/classes/app_config.class.dart';

void main() async {
  final AppConfig prodAppConfig =
      AppConfig(appName: 'PayeTonKawa Prod', flavor: 'prod');
  initializeApp(prodAppConfig).then((app) => runApp(app));
}

/*
flutter run -t lib/main_prod.dart  --release --flavor=prod
flutter build appbundle -t lib/main_prod.dart  --flavor=prod
flutter build apk -t lib/main_prod.dart  --flavor=prod

-----FIREBASE CONF
flutterfire config --project=driing-3b5b4 --out=lib/firebase_options_prod.dart --android-app-id=com.doouble.driing_app.prod --ios-bundle-id=com.doouble.driingApp.prod
*/
