import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonsLayout extends StatelessWidget {
  final List<Widget> buttons;
  final Widget child;

  const ButtonsLayout({super.key, required this.buttons, required this.child});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        Positioned(
          bottom: 20,
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: buttons,
            ),
          ),
        )
      ],
    );
  }
}
